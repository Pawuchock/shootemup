﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour
{
		public static float enemySpeed = 1.0f;
		Vector3 pos;
		float x;
		// Use this for initialization
		void Start ()
		{
				{
						x = Random.Range (-5, 5);
						pos = new Vector3 (x, 8, 0);
						transform.position = pos;

				}
		}

		void Update ()
		{
				rigidbody.velocity = -new Vector2 (0.0f, enemySpeed);
			if (transform.position.y < -1) {
			Destroy(gameObject);			}
		}

		// Update is called once per frame

}
